package jrgd.GDMenu;

import org.bukkit.plugin.java.JavaPlugin;

public class Main extends JavaPlugin {

    // It's quiet, too quiet
    @Override
    public void onEnable() {
        // Wow, a single command
        this.getCommand("dosh").setExecutor(new CommandHandler());
        getServer().getPluginManager().registerEvents(new GuiHandler() , this);
    }
    
    @Override
    public void onDisable() {
        
    }
    
}
