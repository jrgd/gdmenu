package jrgd.GDMenu;

import java.lang.reflect.Field;
import java.util.UUID;

import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.inventory.meta.SkullMeta;

import com.mojang.authlib.GameProfile;
import com.mojang.authlib.properties.Property;

public class ItemHandler {
// Skull formatting and creation
    // An efficient method that should supposedly be mostly version agnostic, granted 1.13+
    // No NMS malarkey, Woot!
    
    public static ItemStack createSkull(String url, String uuid, int count) {
        ItemStack head = new ItemStack(Material.PLAYER_HEAD, count);
        if (url.isEmpty())
            return head;

        SkullMeta headMeta = (SkullMeta) head.getItemMeta();
        GameProfile profile = new GameProfile(UUID.fromString(uuid), null);

        profile.getProperties().put("textures", new Property("textures", url));

        try {
            Field profileField = headMeta.getClass().getDeclaredField("profile");
            profileField.setAccessible(true);
            profileField.set(headMeta, profile);

        } catch (IllegalArgumentException | NoSuchFieldException | SecurityException | IllegalAccessException error) {
            error.printStackTrace();
        }
        head.setItemMeta(headMeta);
        return head;
    }
    
    public static ItemStack headBuild(String skin, String uuid, String name) {
        ItemStack thorHead = createSkull(skin, uuid, 1);
        ItemMeta headMeta = thorHead.getItemMeta();
        headMeta.setDisplayName(name);
        thorHead.setItemMeta(headMeta);
        return thorHead;
    }
    
    public static int playerHasItem(Player player, ItemStack item) {
        
        int count = 0;
         
        for (ItemStack stack : player.getInventory().getContents()) {
            
            if (stack != null && stack.isSimilar(item)) {
                count += stack.getAmount();
            }
        }
        return count;
    }
    
    public static void basicTrade(Player p, int input, int output, ItemStack inputItem, ItemStack outputItem) {
        if (p.getInventory().containsAtLeast(inputItem, input)) {
            
            inputItem.setAmount(input);
            outputItem.setAmount(output);
            
            p.getInventory().removeItem(inputItem);
            for(ItemStack is: p.getInventory().addItem(outputItem).values()) {
                p.getLocation().getWorld().dropItem(p.getLocation(), is);
            }
        }
    }
    
    public static void fullTrade(Player p, int input, int output, ItemStack inputItem, ItemStack outputItem) {

        if (p.getInventory().containsAtLeast(inputItem, input)) {
            
            int inputCount = playerHasItem(p, inputItem) - (playerHasItem(p, inputItem) % input);
            int outputCount = ((inputCount / input) * output);
            inputItem.setAmount(inputCount);
            outputItem.setAmount(outputCount);
            
            p.getInventory().removeItem(inputItem);
            for(ItemStack is: p.getInventory().addItem(outputItem).values()) {
                p.getLocation().getWorld().dropItem(p.getLocation(), is);
            }
        }
    }
    
    public static void fullTrade(Player p, int input, int output, ItemStack inputItem, ItemStack outputItem, int minValue) {

        if (p.getInventory().containsAtLeast(inputItem, minValue)) {
            int inputCount = playerHasItem(p, inputItem) - (playerHasItem(p, inputItem) % input);
            int outputCount = ((inputCount / input) * output);
            inputItem.setAmount(inputCount);
            outputItem.setAmount(outputCount);
            
            p.getInventory().removeItem(inputItem);
            for(ItemStack is: p.getInventory().addItem(outputItem).values()) {
                p.getLocation().getWorld().dropItem(p.getLocation(), is);
            }
        }
    }
}
    
