package jrgd.GDMenu;

import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import jrgd.GDMenu.GuiHandler;;

public class CommandHandler extends GuiHandler implements CommandExecutor {
    // Did anyone expect something out of the ordinary here?
    @Override
    public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
        if (sender instanceof Player) {
            Player player = (Player) sender;
            if (player.hasPermission("gdm.dosh")) {
                buildInventory(player);
            }
            else {
                player.sendMessage("§cInsufficent permissions.");
            }
                
        }
        
        return true;
        
    }
    
}

