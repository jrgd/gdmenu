package jrgd.GDMenu;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import static jrgd.GDMenu.ItemHandler.headBuild;
import static jrgd.GDMenu.ItemHandler.basicTrade;
import static jrgd.GDMenu.ItemHandler.fullTrade;

import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.InventoryHolder;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import com.sun.istack.internal.NotNull;

public class GuiHandler implements InventoryHolder, Listener {
    
// Globals for the entire class
    
    // Inventory list (where all menus should end up going)
    private ArrayList<Inventory> inv = new ArrayList<Inventory>();
    
    // Map for inventory list (it's dangerous to search alone, take this!)
    // Also works as a bind for one menu type per player
    public Map<String, Integer> invMap = new HashMap<String, Integer>();
    
    public final String doshname = "§1Dosh Conversion";
    public Integer menuCounter = 0;
    
    public final String doshskin = "eyJ0ZXh0dXJlcyI6eyJTS0lOIjp7InVybCI6Imh0dHA6Ly90ZXh0dXJlcy5taW5lY3JhZnQubmV0L3RleHR1cmUvZTM2ZTk0ZjZjMzRhMzU0NjVmY2U0YTkwZjJlMjU5NzYzODllYjk3MDlhMTIyNzM1NzRmZjcwZmQ0ZGFhNjg1MiJ9fX0=";
    public final String doshuid = "01234567-89ab-cdef-0123-456789abcdef";
    @NotNull
    public Inventory getInventory(Player p) {
        return inv.get(invMap.get("dosh." + p.getUniqueId().toString()));
    }

// Method to build the Dosh menu contents
    public void DoshData(Player p) {
        
    // Adding items to a list
        // One day, this will just be a for loop pulling from config, or something like that
        
        inv.get(invMap.get("dosh." + p.getUniqueId().toString())).setItem(3, createGuiItem(headBuild(doshskin, doshuid, ""), "§9Diamonds > Dosh", "§a[§eLeft Click§a] §7Single trade", "§a[§eShift §a+ §eLeft Click§a] §7Up to stack of Dosh", "§a[§eRight Click§a] §7Trade all diamonds"));
        inv.get(invMap.get("dosh." + p.getUniqueId().toString())).setItem(4, createGuiItem(Material.BLUE_STAINED_GLASS_PANE, 1, "§aDosh Conversion", "§eDiamonds > Dosh | §b2:1", "§eDosh > Diamonds | §b5:7"));
        inv.get(invMap.get("dosh." + p.getUniqueId().toString())).setItem(5, createGuiItem(Material.DIAMOND, 1, "§9Dosh > Diamonds", "§a[§eLeft Click§a] §7Single trade", "§a[§eShift §a+ §eLeft Click§a] §7Up to stack of diamonds", "§a[§eRight Click§a] §7Trade all Dosh"));
    }

// Method to create GUI items
    private ItemStack createGuiItem(Material material, int count, String name, String...lore) {
        ItemStack item = new ItemStack(material, count);
        ItemMeta meta = item.getItemMeta();
        meta.setDisplayName(name);
        ArrayList<String> metalore = new ArrayList<String>();      

        for(String lorecomments : lore) {
            metalore.add(lorecomments);
        }

        meta.setLore(metalore);
        item.setItemMeta(meta);
        return item;
    }
    
    private ItemStack createGuiItem(ItemStack item, String name, String...lore) {
        ItemMeta meta = item.getItemMeta();
        meta.setDisplayName(name);
        ArrayList<String> metalore = new ArrayList<String>();      

        for(String lorecomments : lore) {
            metalore.add(lorecomments);
        }

        meta.setLore(metalore);
        item.setItemMeta(meta);
        return item;
    }
    
// Method to assemble and open an inventory for a player
    // Some things should be swapped out in the future to make room for more than just dosh menu
    public void buildInventory(Player p) {
        
        // Make new inventory and add to map
        invMap.put(("dosh." + p.getUniqueId().toString()), menuCounter);
        Inventory tempInv = Bukkit.createInventory(this, 9, doshname);
        inv.add(menuCounter,tempInv);
        
        menuCounter++;
        
    // Run DoshData, assemble contents
        DoshData(p);
    
    // Open only when complete (otherwise you let the magic spill out)
        p.openInventory(inv.get(invMap.get("dosh." + p.getUniqueId().toString())));
    }
    
    //Why is this all the way down here?
    
// Check for clicks on items in a dosh menu and ends them
    @EventHandler
    public void onInventoryClick(InventoryClickEvent e) {
        Player p = (Player) e.getWhoClicked();
        for (Inventory exec : inv)
            if (exec.getHolder() != this) {
            return;
        }
        // Bad inventory identification, change this to match hashmaps or something later
        if (e.getView().getTitle() != doshname) {
            return;
        }
        
           e.setCancelled(true);
           
           // I smell conflicting/duplicate head creation
           
        ItemStack clickedItem = e.getCurrentItem();

    // Verify current item is not null, otherwise things crash (and burn!)
        if (clickedItem == null || clickedItem.getType() == Material.AIR) return;

    // Check which slot is clicked and commit trades
        switch (e.getRawSlot()) {
        case 3:
        
            switch (e.getClick()) {
            case LEFT:
                basicTrade(p, 2, 1, new ItemStack(Material.DIAMOND), headBuild(doshskin, doshuid, "§6Dosh"));
                break;
            case SHIFT_LEFT:
                basicTrade(p, 128, 64, new ItemStack(Material.DIAMOND), headBuild(doshskin, doshuid, "§6Dosh"));
                break;
            case RIGHT:
                fullTrade(p, 2, 1, new ItemStack(Material.DIAMOND), headBuild(doshskin, doshuid, "§6Dosh"));
                break;
            default:
                break;
            }
            break;
        
        case 5:
        
            switch (e.getClick()) {
            case LEFT:
                basicTrade(p, 5, 7, headBuild(doshskin, doshuid, "§6Dosh"), new ItemStack(Material.DIAMOND));
                break;
            case SHIFT_LEFT:
                basicTrade(p, 45, 63, headBuild(doshskin, doshuid, "§6Dosh"), new ItemStack(Material.DIAMOND));
                break;
            case RIGHT:
                fullTrade(p, 5, 7, headBuild(doshskin, doshuid, "§6Dosh"), new ItemStack(Material.DIAMOND));
                break;
            default:
                break;
            }
            break;
        }
    }
    
    @Override
    public Inventory getInventory() {
        return null;
    }
}
 
